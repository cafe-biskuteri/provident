
package cafe.biskuteri.provident;

import java.awt.Image;
import java.awt.Toolkit;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

class
Types {

	public static Object
	parseObject(InputStream inputStream, String contentType)
	{
		try
		{
			if (contentType.equals("application/json"))
				return readJsonObject(inputStream);
	
			if (includes(contentType, "image/jpeg", "image/png"))
				return readImageIO(inputStream);
	
			if (contentType.equals("image/gif"))
				return readToolkitImage(inputStream);

			return null;
		}
		catch (JsonException eJson) { return null; }
		catch (IOException eIo) { return null; }
	}

//	 -	-%-	 -

	private static JsonObject
	readJsonObject(InputStream inputStream)
	throws JsonException
	{
		return Json.createReader(inputStream).readObject();
	}

	private static Image
	readImageIO(InputStream inputStream)
	throws IOException
	{
		return ImageIO.read(inputStream);
	}

	private static Image
	readToolkitImage(InputStream inputStream)
	throws IOException
	{
		return Toolkit.getDefaultToolkit()
			.createImage(copyToFileThenGetURL(inputStream));
	}


	private static URL
	copyToFileThenGetURL(InputStream is)
	{
		try
		{
			File tempFile = File
				.createTempFile("provident-types", null);
			tempFile.deleteOnExit();

			OutputStream os = new FileOutputStream(tempFile);
			byte[] buffer = new byte[4096];
			int count; while ((count = is.read(buffer)) != -1)
				os.write(buffer, 0, count);
			os.close();

			return tempFile.toURI().toURL();
		}
		catch (IOException eIo) { return null; }
	}

	private static boolean
	includes(Object item, Object... array)
	{
		for (Object item2: array)
			if (item2.equals(item))
				return true;
		return false;
	}

}