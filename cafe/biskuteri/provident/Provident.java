
package cafe.biskuteri.provident;

import java.util.Map;
import java.util.HashMap;
import java.net.URL;
import java.io.IOException;

import java.io.InputStream;

class
Provident {

	private Map<URL, UsageEntry>
	usage;

	private DirectoryIO
	directoryIO;

//	---%-@-%---

	public Object
	load(URL url, int countUntilPermanent)
	throws IOException
	{
		UsageEntry entry = usage.get(url);
		if (entry == null)
		{
			entry = new UsageEntry();
			entry.url = url;
			entry.reloadCount = 0;
			usage.put(url, entry);
		}

		boolean newEntry = entry.reloadCount == 0;
		++entry.reloadCount;
		boolean makePermanent = entry
			.reloadCount > countUntilPermanent;

		if (entry.permanent != null)
		{
			return entry.permanent.parsed;
		}
		else if (newEntry)
		{
			Data impermanent = loadFromInternet(url);
			// 'transient' is a reserved keyword.
			if (makePermanent) entry.permanent = impermanent;
			return impermanent.parsed;
		}
		else
		{
			String filename = DirectoryIO.createSafeFilename(url);
			Data impermanent = loadFromDisk(filename);
			if (makePermanent) entry.permanent = impermanent;
			return impermanent.parsed;
		}
	}

//	 -	-%-	 - 

	private Data
	loadFromInternet(URL url)
	throws IOException
	{
		return null;
	}

	private Data
	loadFromDisk(String filename)
	throws IOException
	{
		InputStream inputStream = directoryIO
			.createInputStream(filename);
		Object parsed = Types.parseObject(inputStream, null);
		/*
		* (未) A content type is mandatory for coherent
		* parsing. We have to get a content type from a
		* file extension if it comes to that. We cannot
		* advance right now without this information.
		*/

		return null;
	}

//	---%-@-%---

	private static class
	Data {

		Object
		parsed;

		String
		contentType;

	}

	private static class
	UsageEntry {

		URL
		url;

		Data
		permanent;

		int
		reloadCount;

	}

//	---%-@-%---

	public
	Provident(String directoryPath)
	throws IOException
	{
		usage = new HashMap<>();

		directoryIO = new DirectoryIO(directoryPath);
	}

} 
