
package cafe.biskuteri.provident;

import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;

class
DirectoryIO {

	private File
	directory;

//	---%-@-%---

	public InputStream
	createInputStream(String filename)
	throws IOException
	{
		try
		{
			File file = new File(directory, filename);
			return new FileInputStream(file);
		}
		catch (FileNotFoundException eFnf)
		{
			throw new IOException(eFnf);
		}
	}

//	 -	-%-	 -

	public static String
	createSafeFilename(URL url)
	{
		return toBase64(url.toString());
	}

//	 -	-%-	-

	private static String
	toBase64(String string)
	{
		Base64.Encoder enc = Base64.getUrlEncoder();
        return enc.encodeToString(string.getBytes());
	}

//	 ---%-@-%---

	public
	DirectoryIO(String directoryPath)
	throws IOException
	{
		directory = new File(directoryPath);
		ensureWritableDirectory(directory);
	}

//	 -	-%-	 -

	private static void
	ensureWritableDirectory(File directory)
	throws IOException
	{

	}

}